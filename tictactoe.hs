
{-# LANGUAGE OverloadedStrings, LambdaCase #-}
import SDL
import Linear (V4(..))
import Control.Monad
import Data.Maybe
import GHC.Int (Int32)
import Data.Text (pack)
import GHC.Word (Word8)
import Data.Vector.Storable (singleton, cons)
import Control.Arrow ((***), (&&&))
import Data.Array
import Data.List.Split
import Control.Monad.State


data Tic = None|X|O deriving (Show, Eq)

type Tac = Array (Int, Int) Tic

type Toe = State Tac Tic

other :: Tic -> Tic
other X = O
other O = X
other None = None

color :: Tic -> V4 Word8
color X = V4 255 0 0 255
color O = V4 0 0 255 255
color None = V4 20 20 20 255

main :: IO()
main = do
        initializeAll
        window <- createWindow "TicTacHaskell" defaultWindow {windowInitialSize = V2 300 300}
        renderer <- createRenderer window (-1) defaultRenderer
        rendererDrawBlendMode renderer $= BlendAlphaBlend
        let intial = listArray ((0,0), (2,2)) $ repeat None
        apploop renderer (intial, X)
        destroyRenderer renderer
        destroyWindow window
        quit


apploop :: Renderer -> (Tac, Tic) -> IO()
apploop renderer state@(board, turn) = do
    (shouldQuit, input) <- getInput

    -- clear board
    rendererDrawColor renderer $= color None
    clear renderer
    -- render lines
    rendererDrawColor renderer $= V4 100 100 100 255
    forM_ [0..2] (\i -> do
                let ph = V2 (100 * i) 0
                let pv = V2 0 (100 * i)
                drawLine renderer (P ph) (P $ ph + V2 0 300)
                drawLine renderer (P pv) (P $ pv + V2 300 0)
            )
    --render board contents    
    forM_  (assocs board) $ uncurry $ rendercell renderer

    -- draw winner symbol
    let w = winner board
    unless (elem None board && None == w) $ do
        rendererDrawColor renderer $= V4 20 20 20 30
        fillRect renderer Nothing
        rendererDrawColor renderer $= color w
        if w == None then
            drawoverlay renderer
        else do
            rendererScale renderer $= V2 3 3
            rendercell renderer (0, 0) w
            rendererScale renderer $= V2 1 1

        -- print $ winMessage w
        -- delay 1000
        -- showSimpleMessageBox Nothing Information "Result" $ pack $winMessage w

    -- render
    present renderer

    unless shouldQuit $ apploop renderer $
        case input of
                Just coord -> guess state coord
                Nothing -> state
                

getInput :: IO(Bool, Maybe (Int, Int))
getInput = do
        events <- fmap eventPayload <$> pollEvents
        let shouldQuit = any eventShouldQuit events
            candidate = listToMaybe $ catMaybes $ getClickCoords <$> events

        return (shouldQuit, candidate)
    where
        eventShouldQuit :: EventPayload -> Bool
        eventShouldQuit (KeyboardEvent keyboardEvent) = keyboardEventKeyMotion keyboardEvent == Pressed &&
                                                        keysymKeycode (keyboardEventKeysym keyboardEvent) == KeycodeQ
        eventShouldQuit QuitEvent                   = True
        eventShouldQuit _                           = False

        getClickCoords :: EventPayload -> Maybe (Int, Int)
        getClickCoords (MouseButtonEvent mouseevent)= case mouseButtonEventMotion mouseevent of
                                                            Released    -> Just ( coords $ mouseButtonEventPos mouseevent)
                                                            _           -> Nothing
        getClickCoords _                            =    Nothing

coords :: Point V2 Int32 -> (Int, Int)
coords (P (V2 x y)) = join (***) fromIntegral (div x 100, div y 100)

guess :: (Tac, Tic) -> (Int, Int) -> (Tac, Tic)
guess (board, turn) pos
    | elem pos $ indices board  =   case board ! pos of
                                        None -> (board // [(pos, turn)], other turn)
                                        _ -> (board, turn)
    | otherwise                 =   (board, turn)


rendercell :: Renderer -> (Int, Int) -> Tic -> IO()

rendercell ren _ None = return ()

rendercell ren (x, y) X = do
    rendererDrawColor ren $= color X
    let p1 = V2 (fromIntegral $ 100 * x) (fromIntegral $ 100 * y) + V2 20 20
        p2 = p1 + V2 60 60
        p3 = p1 + V2 60 0
        p4 = p1 + V2 0 60
    do
        drawLine ren (P p1) (P p2)
        drawLine ren (P p3) (P p4)

rendercell ren (x, y) O = do
    rendererDrawColor ren $= color O
    let p = V2 (fromIntegral $ 100 * x) (fromIntegral $ 100 * y) + V2 20 20
    drawRect ren $ Just $ Rectangle (P p) (V2 61 61)

drawoverlay :: Renderer -> IO()
drawoverlay renderer = do
        rendererScale renderer $= V2 3 3
        rendererDrawColor renderer $= color X
        -- drawLines renderer $ cons (P $ V2 80 20) $ cons (P $ V2 50 50) $ singleton $ P $ V2 80 80
        -- rendererDrawColor renderer $= color O
        -- drawLines renderer $ cons (P $ V2 50 20) $ cons (P $ V2 20 20) $ cons (P $ V2 20 80) $
        --    singleton $ P $ V2 50 80
        drawLines renderer $ cons (P $ V2 80 20) $ cons (P $ V2 50 50) $ singleton $ P $ V2 20 20
        drawLine renderer (P $ V2 50 20) $ P $ V2 50 80
        --rendercell X renderer 0
        rendercell renderer (0, 0) O
        rendererScale renderer $= V2 1 1


winMessage :: Tic -> String
winMessage None = "It's a Tie!"
winMessage a = show a ++ " has Won! :)"


-- Get the rows, the columns, the diagonals -> put all of them in one list -> get the dominant element for each -> find some non-None element
winner :: Tac -> Tic
winner = foldl1 something . fmap (foldl1 dom) . concat . sequence [id, trans, sequence [diag, diag . reverse]] . chunksOf 3 . elems
    where   dom a b 
                | a == b = b
                | otherwise = None

            something None a = a
            something a _ = a


-- transpose of nested list
trans = foldr (zipWith (:)) (repeat [])
           
-- diagonal of nested list
diag :: [[a]] -> [a]
diag (a:rest)   = head a : diag (fmap tail rest)
diag []         = []

